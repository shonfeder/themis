open! Base

let () =
  Alcocheck.log ~level:`Verbose;
  Alcotest.run "ABT" [
    Calc.unit_tests;
    Calc.property_tests;
    Lambda.unit_tests;
    Lambda.property_tests;
  ];
