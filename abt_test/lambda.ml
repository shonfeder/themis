open! Base

module Term = struct
  type 'a t =
    (** [Lam]bdas merely wrap ABT [Abs]tractions *)
    | Lam of 'a
    | App of 'a * 'a
  [@@deriving map, fold]

  let to_string = function
    | Lam s -> s
    | App (a, b) -> Printf.sprintf "(%s %s)" a b

  let equal ~equal a b =
    match a, b with
    | App (m, n), App (m', n') -> equal m m' && equal n n'
    | Lam m,      Lam n        -> equal m n
    | _,          _            -> false
end

include Term

module Syntax = Abt.Syntax.Make (Abt.Var.String) (Term)
open Syntax

let var : string -> t      = var
let app : t -> t -> t      = fun m n -> term (App (m, n))
let lam : string -> t -> t = fun u m -> term (Lam (abs u m))

exception MalformedLambda of Syntax.t
exception Impossible

let rec eval t =
  match unwrap t with
  | Var _ | Term (Lam _) -> t
  | Term (App (m, n))    -> apply (eval m) (eval n) |> Option.value ~default:t
and apply m n =
  match unwrap m with
  | Var _        -> None
  | Term (Lam l) -> let (x, e) = Syntax.unabs_exn l in Some (subst n x e)
  | Term _       -> apply (eval m) (eval n)

(*********
 * TESTS *
 *********)

(* Helpers for constructing unit tests *)

let check_term name =
  let t_pp = Fmt.of_to_string to_string in
  let testable = Alcotest.testable t_pp equal in
  Alcocheck.unit name testable

let check_alpha_equiv name m n =
  check_term (Printf.sprintf "%s: %s =α %s" name (to_string m) (to_string n))
    ~actual:m ~expected:n

let refute_alpha_equiv name m n =
  let t_pp = Fmt.of_to_string to_string in
  let testable = Alcotest.neg (Alcotest.testable t_pp equal) in
  let name = Printf.sprintf "%s: %s ≠α %s" name (to_string m) (to_string n) in
  Alcocheck.unit name testable ~expected:m ~actual:n

let test_set name =
  let pp = Fmt.of_to_string (fun x ->
      x |> Set.to_list |> List.map ~f:Var.to_string |> String.concat ~sep:", ")
  in
  let testable = Alcotest.testable pp Set.equal in
  Alcocheck.unit name testable

(* Terms that are reused frequently in the tests *)
let aequiv_lhs_1 = (app
                      (lam "x"
                         (app
                            (var "x")
                            (lam "z" (app (var "x") (var "y")))))
                      (var "z"))

let aequiv_lhs_2 = lam "x" (lam "y" (app (app (var "x") (var "z")) (var "y")))

(* The unit tests *)
let unit_tests = Alcocheck.unit_suite "Lambda unit tests" [

    test_set "gathering all free variables"
      ~expected:(Set.of_list (module Var) ["x"; "y"; "z"])
      ~actual:begin
        let term = (lam "a" (app (app (var "x") (var "y")) (app (var "a") (var "z"))))
        in
        Syntax.freevars term
      end;

    check_term "substitution avoids capture"
      ~actual:(subst (var "z") "x" (lam "x" (var "x")))
      ~expected:(lam "x" (var "x"));

    check_term "substitution on free vars in a lambda"
      ~actual:(subst (var "z") "x" (lam "y" (app (var "y") (var "x"))))
      ~expected:(lam "y" (app (var "y") (var "z")));

    check_term "mixed substitutions"
      ~actual:(subst (var "z") "x" (app (var "x") (lam "x" (app (var "x") (var "y")))))
      ~expected:(app (var "z") (lam "x" (app (var "x") (var "y"))));

    check_term "eval id"
      ~actual:(eval (app (lam "x" (var "x")) (var "z")))
      ~expected:(var "z");

    check_term "eval discard"
      ~actual:(eval (app (lam "x" (lam "x" (var "x"))) (var "z")))
      ~expected:(lam "x" (var "x"));

    (* TODO fix so that original variable names are recovered? *)
    check_term "eval insert"
      (* NOTE The renaming of the binding z => z' to avoid collision *)
      ~expected:(lam "z'" (var "z"))
      ~actual:(eval (app (lam "x" (lam "z" (var "x"))) (var "z")));

    check_term "simple alpha equivalence"
      ~actual:(lam "y" (app (var "y") (var "x")))
      ~expected:(lam "z" (app (var "z") (var "x")));

    check_term "less simple alpha equivalence"
      ~actual:(lam "x"
                 (app (var "x")
                    (lam "x" (app (var "x") (var "z")))))
      ~expected:(lam "y"
                   (app (var "y")
                      (lam "w" (app (var "w") (var "z")))));

    (* Examples from "Type Theory and Formal Proof" *)
    check_alpha_equiv "Ex 1.5.3 (1) (a)" aequiv_lhs_1
      (app
         (lam "x"
            (app
               (var "x")
               (lam "z" (app (var "x") (var "y")))))
         (var "z"))
    ;

    check_alpha_equiv "Ex 1.5.3 (1) (b)" aequiv_lhs_1
      (app
         (lam "u"
            (app
               (var "u")
               (lam "z" (app (var "u") (var "y")))))
         (var "z"))
    ;

    check_alpha_equiv "Ex 1.5.3 (1) (c)" aequiv_lhs_1
      (app
         (lam "z"
            (app
               (var "z")
               (lam "x" (app (var "z") (var "y")))))
         (var "z"))
    ;

    refute_alpha_equiv "Ex 1.5.3 (1) (d)" aequiv_lhs_1
      (app
         (lam "y"
            (app
               (var "y")
               (lam "z" (app (var "y") (var "y")))))
         (var "z"))
    ;

    refute_alpha_equiv "Ex 1.5.3 (1) (e)" aequiv_lhs_1
      (app
         (lam "z"
            (app
               (var "z")
               (lam "z" (app (var "z") (var "y")))))
         (var "z"))
    ;

    refute_alpha_equiv "Ex 1.5.3 (1) (f)" aequiv_lhs_1
      (app
         (lam "u"
            (app
               (var "u")
               (lam "z" (app (var "u") (var "y")))))
         (var "v"))
    ;

    check_alpha_equiv "Ex 1.5.3 (2) (a)" aequiv_lhs_2
      (lam "v" (lam "y" (app (app (var "v") (var "z")) (var "y"))))
    ;

    check_alpha_equiv "Ex 1.5.3 (2) (b)" aequiv_lhs_2
      (lam "v" (lam "u" (app (app (var "v") (var "z")) (var "u"))))
    ;

    refute_alpha_equiv "Ex 1.5.3 (2) (c)" aequiv_lhs_2
      (lam "y" (lam "y" (app (app (var "y") (var "z")) (var "y"))))
    ;

    refute_alpha_equiv "Ex 1.5.3 (2) (d)" aequiv_lhs_2
      (lam "z" (lam "y" (app (app (var "z") (var "z")) (var "y"))))
    ;
  ]

open QCheck

module Generator = struct
  module VarCheck = Abt.QChecker.Make (Var)
  let var = Gen.map var VarCheck.Generator.Var.t
  let app m n = Gen.map2 app m n
  let lam m = Gen.map2 lam VarCheck.Generator.Var.t m

  let t =
    let open Gen in
    sized @@ fix begin fun self -> function
      | 0 -> var
      | n ->
        let n' = n / 2 in
        oneof [ var
              ; app (self n') (self n')
              ; lam (self n')
              ]
    end
end

module Arbitrary = struct
  let make = make ~print:to_string

  let var = make Generator.var
  let app m n = make (Generator.app m.gen n.gen)
  let term m = make (Generator.lam m.gen)
  let t = make Generator.t

  let two t = pair t t
  let three t = triple t t t
end

let (=) = equal
let (<>) a b = not (equal a b)

let property_tests = Alcocheck.property_suite "Lambda property tests" [
    (* ALPHA EQUIVALENCE *)
    Alcocheck.property
      "alpha equivalence -- compatibility"
      Arbitrary.(three t)
      begin fun (l, m, n) ->
        (m = n) ==> (
          app m l = app n l &&
          app l m = app l n
        )
      end;

    Alcocheck.property
      "alpha equivalence -- reflexivity"
      Arbitrary.t
      begin fun t ->
        t = t
      end;

    Alcocheck.property
      "alpha equivalence -- symmetry"
      Arbitrary.(two t)
      begin fun (m, n) ->
        (m = n) ==> (n = m)
      end;

    Alcocheck.property
      "alpha equivalence -- transitivity"
      Arbitrary.(three t)
      begin fun (l, m, n) ->
        (l = m && m = n) ==> (l = n)
      end;
  ]
