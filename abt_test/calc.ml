open! Base

(** A simple calculator language for testing the ABT library

    This takes advantage of variable substitution, but does not utilize the
    variable binding and scoping behavior enabled by ABTs. There is therefor
    only a single scope for all variables in an expression. *)

module Op = struct
  type 'a t =
    | Num of int
    | Plus of 'a * 'a
    | Minus of 'a * 'a
    | Let of (string * 'a) * 'a
  [@@deriving fold, map]

  let equal ~equal a b = match a, b with
    | Num a, Num b -> a = b
    | Plus (a, b), Plus (a', b') -> equal a a' && equal b b'
    | Minus (a, b), Minus (a', b') -> equal a a' && equal b b'
    | Let ((x, y), e), Let ((x', y'), e') -> String.equal x x' && equal y y' && equal e e'
    | _, _ -> false

  let to_string = function
    | Num n -> Int.to_string n
    | Plus (m, n) -> Printf.sprintf "(%s + %s)" m n
    | Minus (m, n) -> Printf.sprintf "(%s - %s)" m n
    | Let ((x, y), e) -> Printf.sprintf "let %s = %s in\n %s" x y e
end

open Op
include Abt.Syntax.Make (Abt.Var.String) (Op)

let num : int -> t = fun n -> term (Num n)
let plus : t -> t -> t = fun m n -> term (Plus (m, n))
let minus : t -> t -> t = fun m n -> term (Minus (m, n))
let let_in : (Var.t * t) -> t -> t = fun bind e -> term (Let (bind, e))

exception InvalidType

let rec eval : t -> t = fun t -> match unwrap t with
  | Var _ | Term (Num _) -> t
  | Term (t) -> match t with
    | Plus (m, n) -> eval_plus m n
    | Minus (m, n) -> eval_minus m n
    | Let ((x, y), e) -> eval_let_in (x, y) e
    | _ -> raise InvalidType
and eval_plus m n =
  match unwrap (eval m), unwrap (eval n) with
  | Term (Num m), Term (Num n) -> num (m + n)
  | _, _ -> raise InvalidType
and eval_minus m n =
  match unwrap (eval m), unwrap (eval n) with
  | Term (Num m), Term (Num n) -> num (m - n)
  | _, _ -> raise InvalidType
and eval_let_in (x, y) e =
  subst y x e |> eval


(*********
 * TESTS *
 *********)

open QCheck

let operators = [((+), plus); ((-), minus)]

let property_tests = Alcocheck.property_suite "Calc property tests" [

    Alcocheck.property
      "operator evaluation is arithmetic"
      (triple (oneofl operators) int int)
      begin fun ((arith, calc), m, n) ->
        let expected = num (arith m n)
        and actual = eval @@ calc (num m) (num n)
        in
        equal expected actual
      end;

    Alcocheck.property
      "nested operator evaluation is arithmetic"
      (quad (oneofl operators) int int int)
      begin fun ((arith, calc), l, m, n) ->
        let expected = num (arith l (arith m n)) in
        let actual = eval @@ calc (num l) (calc (num m) (num n))
        in
        equal expected actual
      end;

    Alcocheck.property
      "operator evaluation with variable substitution is arithmetic"
      (triple (oneofl operators) int int)
      begin fun ((arith, calc), m, n) ->
        let expected = num (arith m n)
        and actual = eval @@ let_in ("x", num m) (calc (var "x") (num n))
        in
        Stdio.printf "actual: %s\nexpected: %s\n" (actual |> to_string) (expected |> to_string);
        equal expected actual
      end;
  ]

let unit_tests = Alcocheck.unit_suite "Calc unit tests" [

    Alcocheck.unit "string conversion of number" Alcotest.string
      ~expected:"4"
      ~actual:(num 4 |> to_string);

    Alcocheck.unit "string conversion of compound term" Alcotest.string
      ~expected:"(5 + 7)"
      ~actual:(plus (num 5) (num 7) |> to_string);
  ]
