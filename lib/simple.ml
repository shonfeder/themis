open! Base
module Option = Option_ext

module Type = struct
  (** Definition 2.2.1 *)
  type t =
    | V of string
    | Arr of t * t
  [@@deriving eq, ord, sexp]

  let rec to_string = function
    | V v -> v
    | Arr (a, b) ->
      let a_str = match a with
        | Arr _ -> Printf.sprintf "(%s)" (to_string a)
        | _ -> to_string a
      in
      Printf.sprintf "%s -> %s" a_str (to_string b)
end

module Term = struct
  module Op = struct
    type 'a t =
      (* The type corresponds to the bound variable *)
      | Lam of Type.t * 'a
      | App of 'a * 'a
    [@@deriving map, fold]

    let to_string = function
      | Lam (_, s) -> s
      | App (a, b) -> Printf.sprintf "(%s %s)" a b

    let equal ~equal a b =
      match a, b with
      | App (m, n), App (m', n')   -> equal m m' && equal n n'
      | Lam (m_t, m), Lam (n_t, n) -> Type.equal m_t n_t && equal m n
      | _,          _              -> false
  end

  module Syntax = Abt.Syntax.Make (Abt.Var.String) (Op)
  include Syntax

  let var : Var.t -> t =
    fun x -> var x
  let app : t -> t -> t =
    fun m n -> term (App (m, n))
  let lam : Var.t -> Type.t -> t -> t =
    fun u t m -> term (Lam (t, abs u m))

  let arg_type_of_lam : t -> Type.t option = fun l ->
    match unwrap l with
    | Term (Lam (t, _)) -> Some t
    | _ -> None (* l must be an abstraction *)

  let rec eval t =
    match unwrap t with
    | Var _ | Term (Lam _) -> t
    | Term (App (m, n))    -> apply (eval m) (eval n) |> Option.value ~default:t
  and apply m n =
    match unwrap m with
    | Var _             -> None
    | Term (Lam (_, l)) -> let (x, e) = Syntax.unabs_exn l in Some (subst n x e)
    | Term _            -> apply (eval m) (eval n)

  let unabs_term t = match unwrap t with
    | Term (Lam (t, e)) -> let (v, e) = Syntax.unabs_exn e in Some (t, v, e)
    | _ -> None
end (* Term *)

module Derivation = struct

  include Derivation.Make (Term) (Type)
  open Option.Let_syntax

  (** Definition 2.4.4: [Intro] and [Elim] run the derivations top-to-bottom and
      bottom-to-top, respectively *)

  module Intro : sig
    val var : Ctx.t -> statement -> judgment option
    val app : Ctx.t -> statement -> statement -> judgment option
    val abs : Ctx.t -> Term.Var.t -> statement -> judgment option
  end = struct
    let var ctx stm =
      let+ stm = Ctx.find_statement ctx stm in
      {ctx; stm}

    let app ctx m n =
      let* m_arg_type = Term.arg_type_of_lam m.trm
      in
      if Type.equal n.typ m_arg_type then
        let stm = {trm = Term.app m.trm n.trm ; typ = n.typ} in
        Some {ctx; stm}
      else
        None

    let abs ctx x_name m =
      let x_trm = Term.var x_name in
      let+ {ctx; stm = x} = take_statement ctx x_trm in
      let stm = {trm = Term.lam x_name x.typ m.trm ; typ = Type.Arr (x.typ, m.typ) }
      in
      {ctx; stm}
  end

  module Elim : sig

    (*
            C
      ---------------
      C, x: t |- x: t
    *)
    val var : Ctx.t -> statement -> Ctx.t option
    val app : Ctx.t -> statement -> (judgment * judgment) option
    val lam : Ctx.t -> statement -> judgment option
  end = struct

    let var ctx stm =
      let* {ctx; stm = stm'} = take_statement ctx stm.trm in
      if equal_statement stm stm' then
        Some ctx
      else
        None

    let app ctx stm = match Term.unwrap stm.trm with
      | Term (App (m, n)) ->
        let+ n_type = Term.arg_type_of_lam m in
        let m_type = Type.Arr (n_type, stm.typ) in
        let m = {ctx; stm = {trm = m; typ = m_type}} in
        let n = {ctx; stm = {trm = n; typ = n_type}}
        in
        (m, n)
      | _ -> None

    let lam ctx stm =
      let* (x_typ, x_name, m) = Term.unabs_term stm.trm in
      let x = Term.var x_name in
      let stm = {trm = m; typ = stm.typ} in
      let+ ctx = Ctx.add ctx x x_typ
      in
      {ctx; stm}

  end

  let derive : judgment -> t option = fun j ->
    let rec find ({ctx; stm} as conclusion) =
      List.fold ~f:Option.first_some ~init:None
        [
          begin
            let* judgment = Elim.lam ctx stm in
            let+ premise = find judgment
            in
            { premises = Some [premise]
            ; conclusion }
          end;

          begin
            let* (m, n) = Elim.app ctx stm in
            let+ m_deriv = find m
            and+ n_deriv = find n
            in
            { premises = Some [m_deriv; n_deriv]
            ; conclusion }
          end;

          begin
            let+ _ = Elim.var ctx stm
            in
            { premises = None
            ; conclusion }
          end
        ]
    in
    find j
end


module Check : sig
  (** Check and infer various properties *)

  val infer_type : Derivation.Ctx.t -> Term.t -> Type.t option
  (** [infer_type ctx term] is [Some type] if [type] can be derived for the
      [term] in the given [ctx]. Otherwise, it is [None]. *)

  val infer_term : Derivation.Ctx.t -> Type.t -> Derivation.judgment option
  (** [infer_term ctx type] is [Some term] if [term] can be synthesized to
      inhabit the [type] in the given [ctx]. Otherwise, it is [None]. *)

  val typing : Derivation.judgment -> bool
  (** [typing j] is [true] if a derivation can be found proving the judgment [j].*)

  val well_typed : ?ctx:Derivation.Ctx.t -> Term.t -> Derivation.t option
  (** [well_typed term] is a [Some derivation] if a type can inferred for [term]
      and a [derivation] can be built demonstrating that the [term] has that
      type in the given [ctx].

      [ctx] defaults to an empty context. *)
end = struct
  open Option.Let_syntax

  let rec infer_type ctx term =
    match Term.unwrap term with
    | Term.Var _ -> Derivation.Ctx.type_of_term ctx term
    | Term.Term (Lam (typ, l)) -> infer_type_of_lam ctx typ l
    | Term.Term (App (m, n)) -> infer_type_of_abs ctx m n

  and infer_type_of_lam ctx source_typ abs =
    let (v, e) = Term.unabs_exn abs in
    let+ target_typ =
      let* ctx' = Derivation.Ctx.add ctx (Term.var v) source_typ in
      infer_type ctx' e
    in
    Type.Arr (source_typ, target_typ)

  and infer_type_of_abs ctx m n =
    let* m_typ = infer_type ctx m in
    let* n_typ = infer_type ctx n in
    match m_typ with
    | Type.V _ -> None
    | Type.Arr (source_typ, target_typ) ->
      Option.some_if (Type.equal source_typ n_typ) target_typ

  let infer_term ctx typ : Derivation.judgment option =
    let open Derivation in
    (* TODO a less ugly way of doing this *)
    let next_var = ref ('a', 0) in
    let make_var (v, n) = String.make 1 v ^ String.make n '\'' in
    let unique_var () =
      let this_var = make_var (!next_var) in
      let (v, ticks) = !next_var in
      let () = if Char.(v >= 'z') then
          next_var := ('a', ticks + 1)
        else
          let v' = Char.to_int v + 1 |> Char.of_int_exn in
          next_var := (v', ticks)
      in
      this_var
    in
    let rec aux ctx typ =
      match (typ : Type.t) with
      | Arr (src, dst) ->
        let v_name = unique_var () in
        let v = Term.var (v_name) in
        let* ctx = Ctx.add ctx v src in
        let* {ctx; stm = {trm = body; _}} = aux ctx dst in
        Some {ctx; stm = Term.lam v_name src body < dst}
      | V _ ->
        match Derivation.Ctx.term_of_type ctx typ with
        | None ->
          let v = Term.var (unique_var ()) in
          let+ ctx = Ctx.add ctx v typ in
          let stm = {trm = v; typ = typ} in
          {ctx; stm}
        | Some trm ->
          let stm = {trm; typ = typ} in
          Some {ctx; stm}
    in
    aux ctx typ

  let typing judgment =
    judgment
    |> Derivation.derive
    |> Option.is_some

  let well_typed ?(ctx=Derivation.Ctx.empty) term =
    let* typ = infer_type ctx term in
    let stm = Derivation.(term < typ) in
    Derivation.derive {ctx; stm}

end
