%{ open L2_syntax %}

%token <string> ID
%token LAM
%token DOT
%token L_PAREN
%token R_PAREN
%token COLON
%token ARROW
%token PI
%token STAR
%token LET
%token EQ
%token IN
%token EOF

%right ARROW

%start <L2_syntax.Term.t option> prog
%%

prog:
  | EOF
    { None }
  | t = terminated(exp, EOF)
    { Some t }
;


let bracketed(exp) ==
  L_PAREN; ~ = exp; R_PAREN; <>

/* TODO Add declarations */
exp:
  | LET; x = ID; COLON; t = typ; EQ; n = terms; IN; m = exp
    { Term.app (Term.lam x t m) n }
  | t = terms
    { t }
;

terms:
  | t = term
    { t }
  | m = terms; n = term
    { Term.app m n }
  | m = terms; t = typs; STAR
    { Term.app2 m t }
  | m = terms; n = bracketed(terms)
    { Term.app m n }
  | app = bracketed(app)
    { app }
  | LAM; v = ID; COLON; t = typs; DOT; m = terms
    { Term.lam v t m }
  | LAM; v = ID; COLON; STAR; DOT; m = terms
    { Term.lam2 v m }
;

app:
  | m = term; n = term
    { Term.app m n }
;

term:
  | v = ID
    { Term.var v }
;

typs:
  | t = typ;
    { t }
  | pi = pi;
    { pi }
  | t = bracketed(pi)
    { t }
;

typ:
  | v = ID
    { Type.var v }
  | arr = arrow
    { arr }
;

pi:
  | PI; x = kind; DOT; t = typs
    { Type.pi x t }
  | PI; x = bracketed(kind); DOT; t = typs
    { Type.pi x t }
;

kind:
  | x  = ID; COLON; STAR
    { x }
;

arrow:
  | a = typ; ARROW; b = typs
    { Type.arr a b }
  | a = bracketed(arrow); ARROW; b = typ
    { Type.arr a b }
;
