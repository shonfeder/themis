open! Base

type token =
  | ID of string
  | LAM
  | DOT
  | L_PAREN
  | R_PAREN
  | COLON
  | ARROW
  | PI
  | STAR
  | LET
  | EQ
  | IN
  | EOF
[@@deriving sexp]

let to_string : token -> string =
  fun t -> Sexp.to_string_hum @@ sexp_of_token t

let is_eof = function
  | EOF -> true
  | _ -> false
