open! Base

open L2_syntax

module Subject = struct
  type t =
    | Term of Term.t
    | Type of Type.t
  [@@deriving eq]

  let to_term = function
    | Term t -> Some t
    | Type _ -> None

  let to_type = function
    | Type t -> Some t
    | Term _ -> None

  let to_string = function
    | Type t -> Type.to_string t
    | Term t -> Term.to_string t

end

module DerivType = struct
  type t =
    | Type of Type.t
    | Kind
  [@@deriving eq]

  let to_type = function
    | Type t -> Some t
    | Kind -> None

  (* kind is just a trace, and we don't need any info, so we can represent it as
     the unit *)
  let to_kind = function
    | Kind -> Some ()
    | Type _ -> None

  let to_string = function
    | Type t -> Type.to_string t
    | Kind   -> "*"
end

include Derivation.Make (Subject) (DerivType)

module Ctx = struct
  include Ctx

  let var_is_declared ctx v = Map.mem ctx (Subject.Type (Type.var v))

  let all_vars_are_declared ctx vars = Set.for_all vars ~f:(var_is_declared ctx)

  (* Strengthens the default requirements for adding to a context with the
     constraint given in Definition 3.4.4 (3), which requires that x : T can
     only be in a context if all free type variables a in T are declared
     previously in the context
  *)
  let add ctx trm typ =
    Stdio.printf "adding %s : %s to context %s\n" (Subject.to_string trm) (DerivType.to_string typ) (Ctx.to_string ctx);
    match (trm : Subject.t), (typ : DerivType.t) with
    | Type _, Kind -> add ctx trm typ
    | Term _, Type typ' ->
      if all_vars_are_declared ctx (Type.freevars typ') then
        add ctx trm typ
      else
        None
    (* No other typing combinations are allowed *)
    | _ -> None
end

open Option_ext.Let_syntax

let rec infer_type ctx term =
  Stdio.printf "inferring type for %s in context %s\n" (Term.to_string term) (Ctx.to_string ctx);
  let* typ = match Term.unwrap term with
    | Term.Var _                 -> Ctx.type_of_term ctx (Subject.Term term) >>= DerivType.to_type
    | Term.Term (Lam (typ, abs)) -> infer_type_of_lam ctx typ abs
    | Term.Term (App (m, n))     -> infer_type_of_abs ctx m n
    | Term.Term (Lam2 abs)       -> infer_type_of_lam2 ctx abs
    (* Should this work for types and higher order abstractions? *)
    | _ -> None
  in
  Stdio.printf "inferred type %s for %s\n" (Type.to_string typ) (Term.to_string term);
  return typ


and infer_type_of_lam ctx source_typ abs =
  let (v, e) = Term.unabs_exn abs in
  let+ target_typ =
    let* ctx' = Ctx.add ctx (Subject.Term (Term.var v)) (DerivType.Type source_typ) in
    infer_type ctx' e
  in
  Type.arr source_typ target_typ

and infer_type_of_abs ctx m n =
  let* m_typ = infer_type ctx m in
  let* n_typ = infer_type ctx n in
  match Type.unwrap m_typ with
  |Type.Term (Type.Op.Arr (src_typ, dst_typ)) ->
    Option.some_if (Type.equal src_typ n_typ) dst_typ
  | _ -> None

and infer_type_of_lam2 ctx abs =
  let (type_v, e) = Term.unabs_exn abs in
  let+ target_typ =
    let* ctx' = Ctx.add ctx (Subject.Type (Type.var type_v)) (DerivType.Kind) in
    infer_type ctx' e
  in
  Type.pi type_v target_typ

let print_rule_start rule = Stdio.printf "trying rule %s\n"  rule
let print_rule_success rule = Stdio.printf "rule %s succeeded\n"  rule

let derive : judgment -> t option = fun conclusion ->
  (** TODO profile and then trim the search space *)
  let rec search ({ctx; stm} as conclusion) =
    Stdio.printf "searching for derivation of %s\n" (judgment_to_string conclusion);
    (* TODO Why aren't logs visible for this location!? *)
    Logs.debug (fun m -> m "searching for derivation of %s\n" (judgment_to_string conclusion));
    List.fold ~f:Option_ext.first_some_thunk ~init:None
      [
        begin
          fun () ->
            print_rule_start "var";
            let+ conclusion = var ctx stm
            in
            print_rule_success "var";
            { premises = None
            ; conclusion }
        end;

        begin
          fun () ->
            print_rule_start "appl";
            let* (m, n) = appl ctx stm in
            let+ m_deriv = search m
            and+ n_deriv = search n
            in
            print_rule_success "appl";
            { premises = Some [m_deriv; n_deriv]
            ; conclusion }
        end;

        begin
          fun () ->
            print_rule_start "abst";
            let* judgment = abst ctx stm in
            let+ premise = search judgment
            in
            print_rule_success "abst";
            { premises = Some [premise]
            ; conclusion }
        end;

        begin
          fun () ->
            print_rule_start "form";
            let+ conclusion = form ctx stm in
            print_rule_success "form";
            { premises = None
            ; conclusion }
        end;

        begin
          fun () ->
            print_rule_start "appl2";
            let* (m, b) = appl2 ctx stm in
            let+ m_deriv = search m
            and+ b_deriv = search b
            in
            print_rule_success "appl2";
            { premises = Some [m_deriv; b_deriv]
            ; conclusion }
        end;

        begin
          fun () ->
            print_rule_start "abst2";
            let* judgment = abst2 ctx stm in
            let+ premise = search judgment
            in
            print_rule_success "abst2";
            { premises = Some [premise]
            ; conclusion }
        end
      ]

  and validate conclusion =
    Stdio.printf "validating %s\n" (judgment_to_string conclusion);
    let+ derivation = search conclusion in
    derivation.conclusion

  and var : Ctx.t -> statement -> judgment option =
    fun ctx stm ->
      let* _ = Subject.to_term stm.trm in
      let* typ = Ctx.type_of_term ctx stm.trm
      in
      Option.some_if (DerivType.equal stm.typ typ)
        (ctx |- stm)

  and appl : Ctx.t -> statement -> (judgment * judgment) option =
    fun ctx stm ->
      let* (m, n) = Subject.to_term stm.trm >>= Term.to_app in
      let* dst_typ = DerivType.to_type stm.typ
      in
      let* src_typ = infer_type ctx n in
      let n_judgment = ctx |- (Subject.Term n < DerivType.Type src_typ) in
      let* _ = validate n_judgment
      in
      let m_typ = Type.arr src_typ dst_typ in
      let m_judgment = ctx |- (Subject.Term m < DerivType.Type m_typ) in
      let* _ = validate m_judgment
      in
      return (m_judgment, n_judgment)

  and abst : Ctx.t -> statement -> judgment option =
    fun ctx stm ->
      let* (src_typ, dst_typ) = DerivType.to_type stm.typ >>= Type.src_dst_of_arr in
      let* (v_name, v_typ, m) = Subject.to_term stm.trm >>= Term.to_lam in
      let* () = Option.some_if (Type.equal v_typ src_typ) () in
      let v = Term.var v_name in
      let* ctx' = Ctx.add ctx (Subject.Term v) (DerivType.Type v_typ)
      in
      validate (ctx' |- (Subject.Term m < DerivType.Type dst_typ))

  and form : Ctx.t -> statement -> judgment option =
    fun ctx stm ->
      let* _ = DerivType.to_kind stm.typ in
      let* b = Subject.to_type stm.trm in
      let all_b_freevars_delcared = Ctx.all_vars_are_declared ctx (Type.freevars b)
      in
      Option.some_if all_b_freevars_delcared
        (ctx |- stm)

  and appl2 : Ctx.t -> statement -> (judgment * judgment) option =
    fun ctx stm ->
      let* (m, b_typ) = Subject.to_term stm.trm >>= Term.to_app2 in
      let* stm_typ = DerivType.to_type stm.typ
      in
      let* b_typ_judgment = form ctx (Subject.Type b_typ < DerivType.Kind)
      in
      let* m_type = infer_type ctx m in
      let* (pi_var, pi_typ) = Type.to_pi m_type in
      let* m_judgment = validate (ctx |- (Subject.Term m < DerivType.Type m_type))
      in
      let dst_typ = Type.subst b_typ pi_var pi_typ
      in
      Option.some_if (Type.equal dst_typ stm_typ)
        (m_judgment, b_typ_judgment)

  and abst2 : Ctx.t -> statement -> judgment option =
    fun ctx stm ->
      let* (type_v, m) = Subject.to_term stm.trm >>= Term.to_lam2 in
      let* (type_v', a_type) = DerivType.to_type stm.typ >>= Type.to_pi in
      let* () = Option.some_if (Type.Var.equal type_v type_v') () in
      let* ctx' = Ctx.add ctx (Subject.Type (Type.var type_v)) DerivType.Kind
      in
      validate (ctx' |- (Subject.Term m < DerivType.Type a_type))

  (* TODO Check that all fre type vars in typ are declared in context *)

  in
  search conclusion
