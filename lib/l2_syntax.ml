open! Base
module Option = Option_ext

module Type = struct
  module Op = struct
    type 'a t =
      | Pi of 'a
      | Arr of 'a * 'a
    [@@deriving map, fold]

    let to_string = function
      (* TODO Print as a PI *)
      | Pi s -> s
      | Arr (s, d) -> Printf.sprintf "(%s -> %s)" s d

    let equal ~equal a b =
      match a, b with
      | Pi a, Pi b -> equal a b
      | Arr (a, a'), Arr (b, b') -> equal a b && equal a' b'
      | _ -> false
  end
  module Syntax = Abt.Syntax.Make (Abt.Var.String) (Op)
  include Syntax

  let var : Var.t -> t = var
  let pi  : Var.t -> t -> t = fun v m -> term (Pi (abs v m))
  let arr : t -> t -> t = fun t1 t2 -> term (Arr (t1, t2))

  let src_dst_of_arr t = match unwrap t with
    | Term (Op.Arr (src, dst)) -> Some (src, dst)
    | _ -> None

  let to_pi : t -> (Var.t * t) option = fun t -> match unwrap t with
    | Term (Op.Pi p) -> Some (unabs_exn p)
    | _ -> None
end

module Term = struct
  module Op = struct
    type 'a t =
      | App of 'a * 'a
      | Lam of Type.t * 'a
      | App2 of 'a * Type.t
      | Lam2 of 'a
    [@@deriving map, fold]

    let to_string = function
      (* TODO render typing info *)
      | Lam (_, s) -> Printf.sprintf "(%s)" s
      | App (a, b) -> Printf.sprintf "(%s %s)" a b
      | App2 (a, t) -> Printf.sprintf "(%s %s)" a (Type.to_string t)
      (* TODO render typing info *)
      | Lam2 s -> s

    let equal ~equal a b =
      match a, b with
      | App (m, n), App (m', n')   -> equal m m' && equal n n'
      | Lam (m_t, m), Lam (n_t, n) -> Type.equal m_t n_t && equal m n
      | App2 (m, t), App2 (m', t') -> equal m m' && Type.equal t t'
      | Lam2 m, Lam2 n             -> equal m n
      | _                          -> false
  end
  module Syntax = Abt.Syntax.Make (Abt.Var.String) (Op)
  include Syntax

  let var : Var.t -> t = var

  let app : t -> t -> t =
    fun m n -> term (App (m, n))

  let lam : Var.t -> Type.t -> t -> t =
    fun v t m -> term (Lam (t, abs v m))

  let app2 : t -> Type.t -> t =
    fun m t -> term (App2 (m, t))

  let lam2 : Var.t -> t -> t =
    fun v m -> term (Lam2 (abs v m))

  let src_type_of_lam : t -> Type.t option = fun l ->
    match unwrap l with
    | Term (Lam (t, _)) -> Some t
    | _ -> None

  let to_app (t:t) = match unwrap t with
    | Term (Op.App (m, n)) -> Some (m, n)
    | _ -> None

  let to_lam (l:t) = match unwrap l with
    | Term (Op.Lam (typ, abs)) -> let (v, e) = unabs_exn abs in Some (v, typ, e)
    | _ -> None

  let to_var (v:t) = match unwrap v with
    | Var v -> Some v
    | Term _ -> None

  let to_app2 (t:t) = match unwrap t with
    | Term (Op.App2 (abs2, typ)) -> Some (abs2, typ)
    |  _ -> None

  let to_lam2 (l:t) = match unwrap l with
    | Term (Op.Lam2 abs) -> Some (unabs_exn abs)
    | _ -> None
end
