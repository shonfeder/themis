{
open Lexing_aux
open L2_token
}

let white   = [ ' ' '\t' ]+
let newline = '\r' | '\n' | "\r\n"
let id      = ['a'-'z' 'A'-'Z' '_']*

rule read =
  parse
    | "\\"    { LAM }
    | "."     { DOT }
    | "("     { L_PAREN }
    | ")"     { R_PAREN }
    | ":"     { COLON }
    | "->"    { ARROW }
    | "Pi"    { PI }
    | "*"     { STAR }
    | "let"   { LET }
    | "="     { EQ }
    | "in"    { IN }
    | id      { ID (Lexing.lexeme lexbuf) }
    | white   { read lexbuf }
    | newline { next_line lexbuf; read lexbuf }
    | _       { raise (SyntaxError ("Unexpected char: " ^ Lexing.lexeme lexbuf)) }
    | eof     { EOF }
