open! Base

let (%) = Fn.compose

module Op = struct
  type 'a t =
    (** [Lam]bdas merely wrap ABT [Abs]tractions *)
    | Lam of 'a
    | App of 'a * 'a
  [@@deriving map, fold]

  let to_string = function
    | Lam s -> s
    | App (a, b) -> Printf.sprintf "(%s %s)" a b

  let equal ~equal a b =
    match a, b with
    | App (m, n), App (m', n') -> equal m m' && equal n n'
    | Lam m,      Lam n        -> equal m n
    | _,          _            -> false
end
open Op

module Syntax = Abt.Syntax.Make (Abt.Var.String) (Op)
include Syntax

let var : Var.t -> t      = fun x -> var x
let app : t -> t -> t     = fun m n -> term (App (m, n))
let lam : Var.t -> t -> t = fun u m -> term (Lam (abs u m))

(** TODO use SEXP representation for debugging errors *)
exception Impossible

let rec eval t =
  match unwrap t with
  | Var _ | Term (Lam _) -> t
  | Term (App (m, n))    -> apply (eval m) (eval n) |> Option.value ~default:t
and apply m n =
  match unwrap m with
  | Var _        -> None
  | Term (Lam l) -> let (x, e) = Syntax.unabs_exn l in Some (subst n x e)
  | Term _       -> apply (eval m) (eval n)

let y_combinator =
  let x = var "x"
  and y = var "y"
  in
  (lam "y" (
      app
        (lam "x" (app y (app x x)))
        (lam "x" (app y (app x x)))))

let fix : t -> t = app y_combinator

(** Definition 1.3.5 *)
let rec subterms : t -> t list = fun t ->
  match unwrap t with
  | Var _ -> [t]
  | Term (App (m, n)) -> t :: subterms m @ subterms n
  | Term (Lam l) ->
    let (_, e) = Syntax.unabs_exn l in
    t :: subterms e

let is_subterm: t -> t -> bool = fun sub t ->
  List.mem ~equal (subterms t) sub

let proper_subterms : t -> t list = fun t ->
  t
  |> subterms
  |> List.filter ~f:(not % (equal t))

let is_closed : t -> bool = fun t ->
  Set.equal (freevars t) Var.Set.empty

module Beta = struct
  open Option.Monad_infix

  (** Finds *something* to reduce, if possible *)
  let one_step t =
    let rec red t = match unwrap t with
      | Term (App (m, n)) ->
        let red_m = match unwrap m with
          | Term (Lam l) ->
            let (x, e) = Syntax.unabs_exn l in
            Some (subst n x e)
          | Term (App (m', n')) ->
            (match red m' >>| fun r -> app r n' with
             | Some t' -> Some t'
             | None -> red n' >>| fun r -> app m' r)
          | Var _ -> None
        in begin
          match red_m with
          | Some _ -> red_m
          | None -> red n >>| fun red_n -> app m red_n
        end
      | Term (Lam l) ->
        let (x, e) = Syntax.unabs_exn l in
        red e >>| fun e' -> lam x e'
      | Var _ -> None
    in
    let result = Option.value ~default:t (red t) in
    Logs.debug (fun m -> m "One-step: %s ~>β %s" (to_string t) (to_string result));
    result

  (* A sequence of one-step reductions up to a final term
     NOTE this does not report every possible contractum, since the
     one_step rule defined above only follows a single possible sequence
     of reductions through a term *)
  let rec contracta t =
    let (=) = equal in
    let red = one_step t in
    if red = t then
      [red]
    else
      t :: contracta red

  let rec red t =
    let (=) = equal in
    let r = one_step t in
    if r = t then r else red r

  (** Definition 1.9.1
      M is in β-normal form if M does not contain any redexes *)
  let rec is_normal t = match unwrap t with
    | Var _ -> true
    | Term (term) -> term_is_normal term
  and term_is_normal term = match term with
    | Lam e -> is_normal e
    | App (m, n) -> match unwrap m with
      | Term (Lam _) -> false
      | Term (App _) | Var _ ->
        is_normal m && is_normal n

  let normalize : t -> (t, t) Result.t = fun t ->
    let r = red t in
    if is_normal r then
      Ok t
    else
      Error r
end
