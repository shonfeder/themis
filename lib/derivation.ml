open! Base

module type Domain = sig
  include Equal.S
  val to_string : t -> string
end

module Make (Subject : Domain) (Type : Domain) = struct
  (** [Make (Subject) (Type)] makes a derivation structure and functions
      building up and manipulating derivations for a calculus consisting of
      typable [Subject]s and [Type]s. [Subject]s are more general than terms,
      since in higher order calculi, types themselves can be typed by sorts
      etc. *)

  open Option_ext.Let_syntax

  (** Definition 2.4.2 *)
  type statement =
    { trm: Subject.t
    ; typ: Type.t
    }
  [@@deriving eq]

  let statement_to_string {trm; typ} =
    Printf.sprintf "%s : %s" (Subject.to_string trm) (Type.to_string typ)

  module Ctx = struct

    (** "a context is meant to give the types of the {i free variables} in a
        lambda-term" (48).

        If the context is only meant to give the type of free variables, then we
        could just use typed variables with the ABT's built-in set of free
        variables gives us the context for free. But this would be to collapse
        the different levels of sign systems, cheating type info into the
        machinery meant to abstract away the issue of variable binding. *)

    (* TODO Define compare on terms so we don't need poly *)
    module Map = Map.Poly

    type t = (Subject.t, Type.t) Map.t

    let to_string t =
      Map.to_alist t
      |> List.map ~f:(fun (trm, typ) -> statement_to_string {trm; typ})
      |> String.concat ~sep:", "
      |> Printf.sprintf "[%s]"

    let equal : t -> t -> bool = Map.equal Type.equal
    let empty : t = Map.empty

    let add : t -> Subject.t -> Type.t -> t option = fun ctx key data ->
      match Map.add ctx ~key ~data with
      | `Duplicate -> None
      | `Ok ctx -> Some ctx

    let type_of_term ctx term : Type.t option = Map.find ctx term

    let term_of_type ctx typ : Subject.t option =
      ctx
      |> Map.to_alist
      |> List.find ~f:(fun (_, typ') -> Type.equal typ' typ)
      |> Option.map ~f:fst

    let find_statement ctx s : statement option =
      let* typ = type_of_term ctx s.trm in
      Option.some_if (Type.equal typ s.typ) s
  end


  type judgment =
    { ctx: Ctx.t
    ; stm: statement
    }

  let judgment_to_string {ctx; stm} =
    Printf.sprintf "%s |- %s" (Ctx.to_string ctx) (statement_to_string stm)


  let (<) trm typ  : statement = {trm; typ}
  let (|-) ctx stm : judgment = {ctx; stm}

  (* premise_1 premise_2 premise_2
     -----------------------------
              conclusion
  *)
  type t =
    { premises : t list option
    ; conclusion: judgment
    }

  let sep _ = String.make 10 '-'

  let rec to_string {premises; conclusion} =
    let premises_str_opt =
      let+ prems = premises in
      List.map ~f:to_string prems
      |> String.concat ~sep:"   "
    in
    let conclusion_str = judgment_to_string conclusion
    in
    match premises_str_opt with
    | None ->
      let bar = sep (String.length conclusion_str) in
      Printf.sprintf "%s\n%s\n" bar conclusion_str
    | Some prems_str ->
      let bar = sep (String.length prems_str) in
      Printf.sprintf "%s\n%s\n%s\n" prems_str bar conclusion_str

  (* C, x : t |- x : _, C'
     --------------------
        C |- x : t, C'
  *)
  let take_statement : Ctx.t -> Subject.t -> judgment option = fun ctx trm ->
    let+ typ = Map.Poly.find ctx trm in
    let ctx = Map.Poly.remove ctx trm in
    {ctx; stm = {trm; typ}}
end
