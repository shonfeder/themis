module Syntax = L2_syntax
module Derivation = L2_derivation

module Parser = Parser.Make (struct
    module Tokens = L2_token
    module Lexer = L2_lexer
    module Ast = L2_syntax.Term
    module Parser = L2_parser
  end)

