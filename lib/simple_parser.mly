%{ open Simple %}

%token <string> ID
%token LAM
%token DOT
%token L_PAREN
%token R_PAREN
%token COLON
%token ARROW
%token LET
%token EQ
%token IN
%token EOF

%right ARROW

%start <Simple.Term.t option> prog
%%

prog:
  | EOF
    { None }
  | t = terminated(exp, EOF)
    { Some t }
;

exp:
  | LET; x = ID; COLON; t = typ; EQ; n = terms; IN; m = exp
    { Term.app (Term.lam x t m) n }
  | t = terms
    { t }

terms:
  | t = term
    { t }
  | m = terms; n = term
    { Term.app m n }
  | m = terms; n = delimited(L_PAREN, terms, R_PAREN)
    { Term.app m n }
;

term:
  | v = ID
    { Term.var v }
  | LAM; x = ID; COLON; t = typ; DOT; m = term
    { Term.lam x t m }
;


typ:
  | v = ID
    { Type.V v }
  | a = typ; ARROW; b = typ
    { Type.Arr (a, b) }
  | t = delimited(L_PAREN, typ, R_PAREN)
    { t }
;
