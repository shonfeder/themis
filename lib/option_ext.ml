include Base.Option
(* TODO Get included into base? *)

module Let_syntax = struct
  include Base.Option.Monad_infix

  let (let+) m f = Let_syntax.Let_syntax.map ~f m
  let (and+) = Let_syntax.Let_syntax.both

  let (let*) m f = Let_syntax.Let_syntax.bind ~f m
  let (and*) = (and+)

  let return = return
end

module Monoid = struct
  module Success : Alg.MONOID with type 'a t = 'a Option.t = struct
    type 'a t = 'a Option.t
    let unit = None
    let op a b = match a, b with
      | None, None -> None
      | Some x, _ | _ , Some x -> Some x
  end
end

type 'a opt_thunk = unit -> 'a option

let first_some_thunk : 'a option -> 'a opt_thunk -> 'a option =
  fun a b -> match a with
    | Some x -> Some x
    | None -> b ()
