#+TITLE: themis

* Syntax
Comment: #

`=` always has the lowest precedence

Some unification
#+BEGIN_SRC ocaml

sum : Int -> Int -> Int
sum = x -> y ->
  z = 2
  (x + y * y) = A + B (* # where A = x B = (y * y)*)
  |-
  (x + y) * z + B;

two = |- (x -> sum(x + x)) 1;
two = 2;
two = 4 (* error, because cannot unify *)

(x -> y -> x ^ (y + 1)) = (a -> b);
    (* where
        a = x
        b = y -> x ^ (y + 1) *)

x = 1;

expr
= b 2
= (y -> x ^ (y + 1)) 2   (* because b = y -> x ^ (y + 1) *)
= (y -> 1 ^ (y + 1)) 2;  (* because x = 1 *)

1 = |- expr = 1 ^ 3;


x = 2
y = 3
5 = |- (x + y);

#+END_SRC
