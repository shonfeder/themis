open Lexing

module type Parser = sig
  module Tokens : sig
    type token
    val is_eof : token -> bool
  end

  module Lexer : sig
    val read : lexbuf -> Tokens.token
  end

  module Ast : sig
    type t
  end

  module Parser : sig
    val prog : (lexbuf -> Tokens.token) -> lexbuf -> Ast.t option
    exception Error
  end
end

module Make (P : Parser) = struct
  let print_position outx lexbuf =
    let pos = lexbuf.lex_curr_p in
    Format.fprintf outx "%s:%d:%d" pos.pos_fname
      pos.pos_lnum (pos.pos_cnum - pos.pos_bol + 1)

  let parse_with_error lexbuf =
    try P.Parser.prog P.Lexer.read lexbuf with
    | Lexing_aux.SyntaxError msg ->
      Format.eprintf "%a: \"%s\"\n" print_position lexbuf msg;
      None
    | P.Parser.Error ->
      Format.eprintf "%a: parsing syntax error\n" print_position lexbuf;
      None

  let rec parse_buf lexbuf =
    match parse_with_error lexbuf with
    | Some t -> t :: parse_buf lexbuf
    | None -> []

  let rec lex_buf lexbuf =
    let t = P.Lexer.read lexbuf in
    if P.Tokens.is_eof t then
      []
    else
      t :: lex_buf lexbuf

  let lex filename =
    let inx = Stdio.In_channel.create filename in
    let lexbuf = Lexing.from_channel inx in
    lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = filename };
    let tokens = lex_buf lexbuf in
    Stdio.In_channel.close inx;
    tokens

  let parse filename =
    let inx = Stdio.In_channel.create filename in
    let lexbuf = Lexing.from_channel inx in
    lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = filename };
    let ast = parse_buf lexbuf in
    Stdio.In_channel.close inx;
    ast

  let parse_string s =
    let lexbuf = Lexing.from_string s in
    lexbuf.lex_curr_p <- { lexbuf.lex_curr_p with pos_fname = "string" };
    parse_buf lexbuf
end

module Simply_typed = Make (struct
    module Tokens = Simple_token
    module Lexer = Simple_lexer
    module Ast = Simple.Term
    module Parser = Simple_parser
  end)
