open! Base

open Themis.Untyped

(* Helpers for constructing unit tests *)

let u = var "u"
let v = var "v"
let w = var "w"
let x = var "x"
let y = var "y"
let z = var "z"

let lambda_term = Alcotest.testable (Fmt.of_to_string to_string) equal

let check_term name = Alcocheck.unit name lambda_term

let check_alpha_equiv name m n =
  check_term (Printf.sprintf "%s: %s =α %s" name (to_string m) (to_string n))
    ~actual:m ~expected:n

let refute_alpha_equiv name m n =
  let name = Printf.sprintf "%s: %s ≠α %s" name (to_string m) (to_string n) in
  Alcocheck.unit name (Alcotest.neg lambda_term) ~expected:m ~actual:n

let test_set name =
  let pp = Fmt.of_to_string (fun x ->
      x |> Set.to_list |> List.map ~f:Var.to_string |> String.concat ~sep:", ")
  in
  let testable = Alcotest.testable pp Set.equal in
  Alcocheck.unit name testable

(* Terms that are reused frequently in the tests *)
let aequiv_lhs_1 = (app
                      (lam "x"
                         (app
                            (var "x")
                            (lam "z" (app (var "x") (var "y")))))
                      (var "z"))

let aequiv_lhs_2 = lam "x" (lam "y" (app (app (var "x") (var "z")) (var "y")))

(* The unit tests *)
let unit_tests = Alcocheck.unit_suite "Lambda unit tests" [

    test_set "gathering all free variables"
      ~expected:(Set.of_list (module Var) ["x"; "y"; "z"])
      ~actual:begin
        let term = (lam "a" (app (app (var "x") (var "y")) (app (var "a") (var "z"))))
        in
        Syntax.freevars term
      end;

    check_term "substitution avoids capture"
      ~actual:(subst (var "z") "x" (lam "x" (var "x")))
      ~expected:(lam "x" (var "x"));

    check_term "substitution on free vars in a lambda"
      ~actual:(subst (var "z") "x" (lam "y" (app (var "y") (var "x"))))
      ~expected:(lam "y" (app (var "y") (var "z")));

    check_term "mixed substitutions"
      ~actual:(subst (var "z") "x" (app (var "x") (lam "x" (app (var "x") (var "y")))))
      ~expected:(app (var "z") (lam "x" (app (var "x") (var "y"))));

    check_term "eval id"
      ~actual:(eval (app (lam "x" (var "x")) (var "z")))
      ~expected:(var "z");

    check_term "eval discard"
      ~actual:(eval (app (lam "x" (lam "x" (var "x"))) (var "z")))
      ~expected:(lam "x" (var "x"));

    (* TODO fix so that original variable names are recovered? *)
    check_term "eval insert"
      (* NOTE The renaming of the binding z => z' to avoid collision *)
      ~expected:(lam "z'" (var "z"))
      ~actual:(eval (app (lam "x" (lam "z" (var "x"))) (var "z")));

    (* ALPHA EQUIVALENCE *)

    check_term "simple alpha equivalence"
      ~actual:(lam "y" (app (var "y") (var "x")))
      ~expected:(lam "z" (app (var "z") (var "x")));

    check_term "less simple alpha equivalence"
      ~actual:(lam "x"
                 (app (var "x")
                    (lam "x" (app (var "x") (var "z")))))
      ~expected:(lam "y"
                   (app (var "y")
                      (lam "w" (app (var "w") (var "z")))));

    (* Examples from "Type Theory and Formal Proof" *)
    check_alpha_equiv "Ex 1.5.3 (1) (a)" aequiv_lhs_1
      (app
         (lam "x"
            (app
               (var "x")
               (lam "z" (app (var "x") (var "y")))))
         (var "z"))
    ;

    check_alpha_equiv "Ex 1.5.3 (1) (b)" aequiv_lhs_1
      (app
         (lam "u"
            (app
               (var "u")
               (lam "z" (app (var "u") (var "y")))))
         (var "z"))
    ;

    check_alpha_equiv "Ex 1.5.3 (1) (c)" aequiv_lhs_1
      (app
         (lam "z"
            (app
               (var "z")
               (lam "x" (app (var "z") (var "y")))))
         (var "z"))
    ;

    refute_alpha_equiv "Ex 1.5.3 (1) (d)" aequiv_lhs_1
      (app
         (lam "y"
            (app
               (var "y")
               (lam "z" (app (var "y") (var "y")))))
         (var "z"))
    ;

    refute_alpha_equiv "Ex 1.5.3 (1) (e)" aequiv_lhs_1
      (app
         (lam "z"
            (app
               (var "z")
               (lam "z" (app (var "z") (var "y")))))
         (var "z"))
    ;

    refute_alpha_equiv "Ex 1.5.3 (1) (f)" aequiv_lhs_1
      (app
         (lam "u"
            (app
               (var "u")
               (lam "z" (app (var "u") (var "y")))))
         (var "v"))
    ;

    check_alpha_equiv "Ex 1.5.3 (2) (a)" aequiv_lhs_2
      (lam "v" (lam "y" (app (app (var "v") (var "z")) (var "y"))))
    ;

    check_alpha_equiv "Ex 1.5.3 (2) (b)" aequiv_lhs_2
      (lam "v" (lam "u" (app (app (var "v") (var "z")) (var "u"))))
    ;

    refute_alpha_equiv "Ex 1.5.3 (2) (c)" aequiv_lhs_2
      (lam "y" (lam "y" (app (app (var "y") (var "z")) (var "y"))))
    ;

    refute_alpha_equiv "Ex 1.5.3 (2) (d)" aequiv_lhs_2
      (lam "z" (lam "y" (app (app (var "z") (var "z")) (var "y"))))
    ;

    (* SUB TERMS *)
    Alcocheck.test "Example 1.3.7 (a)" begin
      is_subterm y y
    end;

    Alcocheck.unit "Example 1.3.7 (b)" Alcotest.(list lambda_term)
      ~actual:(subterms (app x z))
      ~expected:[app x z; x; z]
    ;

    Alcocheck.unit "Example 1.3.7 (c)" Alcotest.(list lambda_term)
      ~actual:(subterms (lam "x" (app x x)))
      ~expected:[lam "x" (app x x); (app x x); x; x]
    ;

    Alcocheck.unit "Example 1.3.7 (d)" Alcotest.(list lambda_term)
      ~actual:(subterms (app (lam "x" (app x x)) (lam "x" (app x x))))
      ~expected:[
        app (lam "x" (app x x)) (lam "x" (app x x));
        lam "x" (app x x); app x x; x; x;
        lam "x" (app x x); app x x; x; x
      ]
    ;

    Alcocheck.unit "Example 1.3.9" Alcotest.(list lambda_term)
      ~actual:(proper_subterms (app y (lam "x" (app x z))))
      ~expected:[y; lam "x" (app x z); (app x z); x; z]
    ;

    (* FREE VARIABLES *)
    Alcocheck.unit "Examples 1.4.2 (a)" Alcotest.(list string)
      ~actual:(freevars (lam "x" (app x y)) |> Set.to_list)
      ~expected:["y"]
    ;

    Alcocheck.unit "Examples 1.4.2 (b)" Alcotest.(list string)
      ~actual:(freevars (app x (lam "x" (app x y))) |> Set.to_list)
      ~expected:["x"; "y"]
    ;

    Alcocheck.test "Closed term 1" begin
      is_closed (lam "x" (lam "y" (lam "z" (app (app x x) y))))
    end;

    Alcocheck.test "Closed term 2" begin
      is_closed (lam "x" (lam "y" (app (app x x) y)))
    end;

    Alcocheck.test "Unclosed term" begin
      (not % is_closed) (lam "x" (app (app x x) y))
    end;

    (* BETA REDUCTION *)
    Alcocheck.unit "Examples 1.8.2 (1)" lambda_term
      ~actual:(Beta.one_step (app (lam "x" (app x (app x y))) z))
      ~expected:(app z (app z y))
    ;

    Alcocheck.unit "Examples 1.8.2 (2): one-step" lambda_term
      ~actual:(Beta.one_step (app (lam "x" (app (lam "y" (app y x)) z)) v))
      ~expected:(app (lam "y" (app y v)) z)
    ;

    begin
      let term = (app (lam "x" (app (lam "y" (app y x)) z)) v) in
      Alcocheck.unit "Examples 1.8.2 (2): β-reduction chain" Alcotest.(list lambda_term)
        ~actual:(Beta.contracta term)
        ~expected:[term; app (lam "y" (app y v)) z; app z v]
    end
    ;
  ]

open QCheck

module Generator = struct
  module VarCheck = Abt.QChecker.Make (Var)
  let var = Gen.map var VarCheck.Generator.Var.t
  let app m n = Gen.map2 app m n
  let lam m = Gen.map2 lam VarCheck.Generator.Var.t m

  let t =
    let open Gen in
    sized @@ fix begin fun self -> function
      | 0 -> var
      | n ->
        let n' = n / 2 in
        oneof [ var
              ; app (self n') (self n')
              ; lam (self n')
              ]
    end
end

module Arbitrary = struct
  let make = make ~print:to_string

  let var = make Generator.var
  let app m n = make (Generator.app m.gen n.gen)
  let term m = make (Generator.lam m.gen)
  let t = make Generator.t

  let two t = pair t t
  let three t = triple t t t
end

let (=) = equal
let (<>) a b = not (equal a b)

let property_tests = Alcocheck.property_suite "Lambda property tests" [
    (* SUB TERMS *)
    Alcocheck.property
      "Lemma 1.3.6: subterm -- reflexivity"
      Arbitrary.t
      begin fun t ->
        is_subterm t t
      end;

    Alcocheck.property
      "Lemma 1.3.6: subterm -- transitivity"
      Arbitrary.(three t)
      begin fun (l, m, n) ->
        (is_subterm l m && is_subterm m n) ==> (is_subterm l n)
      end;

    (* ALPHA EQUIVALENCE *)
    Alcocheck.property
      "alpha equivalence -- compatibility"
      Arbitrary.(three t)
      begin fun (l, m, n) ->
        (m = n) ==> (
          app m l = app n l &&
          app l m = app l n
        )
      end;

    Alcocheck.property
      "alpha equivalence -- reflexivity"
      Arbitrary.t
      begin fun t ->
        t = t
      end;

    Alcocheck.property
      "alpha equivalence -- symmetry"
      Arbitrary.(two t)
      begin fun (m, n) ->
        (m = n) ==> (n = m)
      end;

    Alcocheck.property
      "alpha equivalence -- transitivity"
      Arbitrary.(three t)
      begin fun (l, m, n) ->
        (l = m && m = n) ==> (l = n)
      end;

    (* BETA REDUCTION *)
    Alcocheck.property
      "Definition 1.8.1 (1): one-step β-reduction -- basis"
      Arbitrary.(two t)
      begin fun (m, n) ->
        Beta.one_step (app (lam "x" m) n) = subst n "x" m
      end;

    Alcocheck.property
      "Definition 1.8.1 (1): one-step β-reduction -- compatibility"
      Arbitrary.(two t)
      begin fun (l, m) ->
        let n = Beta.one_step m in
        app (Beta.one_step m) l = app n l
        &&
        app l (Beta.one_step m) = app l n
        &&
        lam "x" (Beta.one_step m) = lam "x" n
      end;

    Alcocheck.property
      "Lemma 1.8.4 (1) ->>β extends ~>β"
      Arbitrary.t
      begin fun m ->
        let n = Beta.one_step m in
        List.mem ~equal (Beta.contracta m) n
      end;

    Alcocheck.property
      "Lemma 1.8.4 (1) ->>β is reflexive"
      Arbitrary.t
      begin fun m ->
        List.mem ~equal (Beta.contracta m) m
      end;

    Alcocheck.property
      "Lemma 1.8.4 (1) ->>β is reflexive"
      Arbitrary.(three t)
      begin fun (l, m, n) ->
        (List.mem ~equal (Beta.contracta l) m && List.mem ~equal (Beta.contracta m) n)
        ==>
        (List.mem ~equal (Beta.contracta l) n)
      end;
  ]
