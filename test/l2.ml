open! Base

open Themis.L2.Syntax

let parse = Themis.L2.Parser.parse_string

let terms_equal = List.equal Term.equal

let terms_to_string ts =
  ts
  |> List.map ~f:Term.to_string
  |> String.concat ~sep:"\n"

let l2_term2 = Alcotest.testable (Fmt.of_to_string terms_to_string) terms_equal

let parsing_unit_tests = Alcocheck.unit_suite "System F parsing unit tests" [
    Alcocheck.unit "can parse simple variable"
      l2_term2
      ~expected:[(Term.var "m")]
      ~actual:(parse "m")
    ;

    Alcocheck.unit "can parse simple application"
      l2_term2
      ~expected:[Term.(app (var "m") (var "n"))]
      ~actual:(parse "m n")
    ;

    Alcocheck.unit "can parse simple abstraction"
      l2_term2
      ~expected:[Term.(lam "x" (Type.var "A") (var "x"))]
      ~actual:(parse "\\x : A. x")
    ;

    Alcocheck.unit "can parse nested abstraction"
      l2_term2
      ~expected:[Term.(lam "x" (Type.var "A") (lam "y" (Type.var "B") (app (var "x") (var "y"))))]
      ~actual:(parse "\\x:A.\\y:B.(x y)")
    ;

    Alcocheck.unit "can parse arrow types"
      l2_term2
      ~expected:[Term.(lam "x" (Type.(arr (var "A") (var "B"))) (var "x"))]
      ~actual:(parse "\\x:A -> B.x")
    ;

    Alcocheck.unit "can parse parametric types"
      l2_term2
      ~expected:[Term.(lam "x" (Type.(pi "a" (arr (var "a") (var "a")))) (var "x"))]
      ~actual:(parse "\\x:(Pi(a:*).a -> a).x")
    ;

    Alcocheck.unit "can parse parametric functions"
      l2_term2
      ~expected:[Term.(lam2 "a" (lam "x" (Type.var "a") (var "x")))]
      ~actual:(parse "\\a:*.\\x:a. x")
    ;

    Alcocheck.unit "can left-associating arrows types"
      l2_term2
      ~expected:[Term.(lam "f" Type.(arr (arr (var "A") (var "B")) (var "B")) (app (var "f") (var "x")))]
      ~actual:(parse "\\f:(A -> B) -> B. f x")
    ;
  ]

open Themis.L2.Derivation

let derivation_unit_tests = Alcocheck.unit_suite "System F parsing unit tests" [
    Alcocheck.test "Can derive 3.5 example"
      begin
        let term = parse "\\a:*. \\f:a -> a. \\x:a. f (f x)" |> List.hd_exn in
        let a = Type.var "a" in
        let typ = Type.(pi "a" (arr (arr a a) (arr a a))) in
        let deriv = derive (Ctx.empty |- (Subject.Term term < DerivType.Type typ)) in
        Option.iter ~f:(fun t -> Stdio.print_endline @@ to_string t) deriv;
        Option.is_some deriv
      end

  ]
