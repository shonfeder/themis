open! Base

let () =
  Alcocheck.log ~level:`Verbose;
  Alcotest.run "Themis" [
    Untyped.unit_tests;
    Untyped.property_tests;
    L2.parsing_unit_tests;
    L2.derivation_unit_tests;
  ]
