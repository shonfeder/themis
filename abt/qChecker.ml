open! Base

(** QCheck generators and arbitrary values for use in testing syntaxes *)

(* TODO Break into sub-package, to reduce dependency footprint? *)
open QCheck

module Ascii = struct
  let lowercase_letter =
    let code_gen = Gen.(97 -- 122) in
    let int_to_char_string x = Char.(x |> of_int_exn |> to_string) in
    Gen.map int_to_char_string code_gen
end

module type OPERATOR_GEN = sig
  type 'a t
  val t : 'a Gen.t -> ('a t) Gen.t
end

module Make (Var : Var.T) = struct
  module Generator  = struct
    module Var = struct
      let t = Gen.map Var.of_string Ascii.lowercase_letter
      let set = t |> Gen.list |> Gen.map (Set.of_list (module Var))
    end
  end

  module Arbitrary = struct
    module Var = struct
      let print = Var.to_string
      let t = make Generator.Var.t ~print
    end
  end
end
