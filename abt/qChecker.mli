open QCheck

module type OPERATOR_GEN = sig
  type 'a t
  val t : 'a Gen.t -> ('a t) Gen.t
end

module Make : functor (Var : Var.T) -> sig
  module Generator : sig
    module Var : sig
      val t : Var.t Gen.t
      val set : Var.set Gen.t
    end
  end

  module Arbitrary : sig
    module Var : sig
      val t : Var.t arbitrary
    end
  end
end
