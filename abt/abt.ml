(** Abstract Binding Tree

    implemented with reference to  https://semantic-domain.blogspot.com/2015/03/abstract-binding-trees.html *)

(** [Make(O:OPERATOR)] creates new syntax from the operator signature specified in [O] *)
module Syntax = Syntax
module Var = Var
module QChecker = QChecker
