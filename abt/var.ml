open! Base

(** Notes form Harper's PFPL *)

(** The concept of a variable is central, and therefore deserves special
    emphasis. *)

module type VAR = sig
  include Equal.S
  include Comparator.S with type t := t
  include Stringable.S with type t := t

  val increment : t -> t
end

module type T = sig
  include VAR

  module Set : sig
    type v := t
    include Alg.MONOID
    val singleton : v -> v t
    val empty : v t
  end with type 'elem t = (t, comparator_witness) Set.t

  type set = t Set.t
end

module Make (V : VAR) : T with type t = V.t = struct
  module Set = struct
    type 'elem t = (V.t, V.comparator_witness) Set.t
    let empty = Set.empty (module V)
    let singleton = Set.singleton (module V)

    let unit : 'elem t = empty
    let op : 'elem t -> 'elem t -> 'elem t = Set.union
  end

  type set = V.t Set.t
  include V
end

module Str : VAR with type t = string = struct
  type t = string
  include Comparator.Make (String)

  let equal = String.equal

  let increment v = v ^ "'"
  let to_string v = v
  let of_string : string -> t = fun v -> v
end

module type TYPE = sig
  include Equal.S
  include Sexpable.S with type t := t
  include Stringable.S with type t := t
  val compare : t -> t -> int
end

(** Variables simply represented as strings *)
module String = Make (Str : VAR with type t = string)
