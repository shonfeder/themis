open! Base

module type T = sig
  module Var : Var.T

  type 'a term
  type 'a f

  type 'a abt = private
    | Var of Var.t
    | Term of 'a term

  (** ABTs include the FUNCTOR theory *)
  include Alg.FUNCTOR with type 'a t = 'a f

  type t

  val is_var  : t -> bool
  val is_abs  : t -> bool
  val is_term : t -> bool

  val to_string : t -> string

  val unwrap : t -> t abt
  val wrap : t abt -> t

  val freevars : t -> Var.set
  val is_combinator : t -> bool

  val var : Var.t -> t
  val abs : Var.t -> t -> t
  val term : t term -> t

  val unabs_exn : t -> (Var.t * t)

  val subst : t -> Var.t -> t -> t
  val equal : t -> t -> bool
end

module type OPERATOR = sig
  include Alg.FUNCTOR
  (* Requires:

     type 'a t
     val map : ('a -> 'b) -> 'a t -> 'b t *)

  val to_string : string t -> string
  val equal : equal:('a -> 'a -> bool) -> 'a t -> 'a t -> bool
  val fold : ('a -> 'b -> 'a) -> 'a -> 'b t -> 'a
end

module Make (Var : Var.T) (Op : OPERATOR) : T
  (** [Make (V:VAR) (O:OPERATOR)] creates a new syntax from the operator
      signature specified in [O] using the representation of variables given in
      [V]. *)
  with type 'a term := 'a Op.t
   and type Var.t = Var.t
= struct
  module Var = Var

  (* [I] stands for internal *)
  type 'a f =
    | IVar of Var.t
    | IAbs of Var.t * 'a
    | ITerm of 'a Op.t

  type 'a abt =
    | Var of Var.t
    | Term of 'a Op.t

  let map f = function
    | IVar x -> IVar x
    | IAbs(x, e) -> IAbs(x, f e)
    | ITerm t -> ITerm (Op.map f t)

  type t = {free_vars: Var.set; term: t f}

  let is_var t = match t.term with
    | IVar _ -> true
    | _ -> false

  let is_abs t = match t.term with
    | IAbs _ -> true
    | _ -> false

  let is_term t = match t.term with
    | ITerm _ -> true
    | _ -> false

  let rec to_string {term; _} = match term with
    | IVar x -> Var.to_string x
    | IAbs (x, e) -> Printf.sprintf "λ%s.%s" (Var.to_string x) (to_string e)
    | ITerm t -> Op.map to_string t |> Op.to_string

  let freevars t = t.free_vars

  (* "A lambda-term M is closed if FV(M) == {}.
     A closed lambda term is also called a combinator" (TTFP 9) *)
  let is_combinator t = Set.equal (freevars t) Var.Set.empty

  (* All free variables from any level of the tree *)
  let tree_freevars t =
    t
    |> Op.map freevars
    |> Op.fold Var.Set.op Var.Set.empty

  let var x =
    { free_vars = Var.Set.singleton x
    ; term = IVar x
    }
  let abs x e =
    { free_vars = Set.remove (freevars e) x
    ; term = IAbs (x, e)
    }
  let term t =
    { free_vars = tree_freevars t
    ; term = ITerm t
    }

  (* Internal wrapping and unwrapping *)
  let iunwrap t = t.term

  exception API_violated

  let unwrap t = match t.term with
    | IVar v -> Var v
    | ITerm t -> Term t
    | IAbs _ -> raise API_violated

  let wrap = function
    | Var x -> var x
    | Term t -> term t

  exception Expected_abstraction of t
  let unabs_exn e = match iunwrap e with
    | IAbs (x, e) -> (x, e)
    | _           -> raise (Expected_abstraction e)

  let rec fresh : Var.set -> Var.t -> Var.t =
    fun vs v -> if Set.mem vs v then fresh vs (Var.increment v) else v

  let rec rename x y t = match t.term with
    | IVar z ->
      if Var.equal x z then
        (Logs.debug (fun m -> m "renaming: %s => %s" (Var.to_string x) (Var.to_string y));
         var y)
      else
        var z
    | IAbs (z, e) ->
      if Var.equal z y || Var.equal z x then
        abs z e
      else
        abs z (rename x y e)
    | ITerm t -> term (Op.map (rename x y) t)

  (** Substitute the sub for x in the target *)
  let rec subst sub x target =
    let log_substitution v fresh_v result =
      Logs.debug (fun m -> m "substitution: %s[%s := %s] ==> %s"
                     (to_string target) (Var.to_string x) (to_string sub) (to_string result));
      if not (Var.equal fresh_v v) then
        Logs.debug (fun m -> m "fresh var: %s => %s" (Var.to_string v) (Var.to_string fresh_v));
    in
    match iunwrap target with
    | IVar z when Var.equal z x -> sub
    | IVar z -> var z
    | ITerm target' -> term (Op.map (subst sub x) target')
    | IAbs (z, e) when Var.equal x z -> abs z e
    | IAbs (z, e) ->
      let z' = fresh (Set.union (freevars sub) (freevars target)) z in
      let e' = subst sub x (rename z z' e) in
      let result = abs z' e' in
      log_substitution z z' result;
      result

  (** [equal] is defined as alpha equivalence, modulo the equality
      relation defined on the operator signature [Term] *)
  let rec equal : t -> t -> bool =
    fun a b ->
    Logs.debug (fun m -> m "checking: %s =α %s" (to_string a) (to_string b));
    match iunwrap a, iunwrap b with
    | IVar a, IVar b -> Var.equal a b
    | ITerm a, ITerm b -> Op.equal ~equal a b
    | IAbs (va, a), IAbs (vb, b) ->
      (* Pick a variable fresh to both bodies, based on the bound var in b *)
      let vf = fresh (Set.union (freevars a) (freevars b)) vb
      in
      (* Rename all bound terms in both bodies to the fresh var *)
      let a' = rename va vf a
      and b' = rename vb vf b
      in
      equal a' b'
    | _, _ -> false

end
