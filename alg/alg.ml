(** Algebraic structures *)
open! Base

module type TRIV = sig
  type t
end

module type FUNCTOR = sig
  type 'a t
  val map : ('a -> 'b) -> 'a t -> 'b t
end

module type MONOID = sig
  type 'a t
  val unit : 'a t
  val op : 'a t -> 'a t -> 'a t
end

module type APPLICATIVE_MONOID = sig
  include MONOID
  include Applicative.S with type 'a t := 'a t
end

module type SET = sig
  include MONOID

  val add : 'elem -> 'elem t -> 'elem t
end

module Mono = struct
  module type MONOID = sig
    type t
    include MONOID with type 'a t := t
  end
end

module Bool = struct
  module Monoid = struct
    module And : Mono.MONOID with type t = bool = struct
      type t = bool
      let unit = true
      let op = (&&)
    end

    module Or : Mono.MONOID with type t = bool = struct
      type t = bool
      let unit = false
      let op = (&&)
    end
  end
end
